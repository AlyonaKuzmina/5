﻿using System;

namespace Кузьмина_Алена_лаб_5___1
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Выберите вид итерации: 1-for; 2-while; 3-do/while");
            var type = Console.ReadLine();

            switch (type)
            {
                case "1":
                    Console.WriteLine("тип for:");
                    Func1();
                    break;
                case "2":
                    Console.WriteLine("тип while:");
                    Func2();
                    break;
                case "3":
                    Console.WriteLine("тип do/while:");
                    Func3();
                    break;
                default:
                    Console.WriteLine("Ошибка. Такого типа нет");
                    break;
            }



            static void Func1()
            {
                double func;

                for (int k = 5; k <= 10; k++)
                {

                    for (double x = -10; x <= 10; x++)
                    {
                        func = (x * k + 1) / Math.Pow(k, 3);
                        Console.Write("{0,12}", Math.Round(func, 4));
                    }
                    Console.WriteLine();
                }
            }

            static void Func2()
            {
                double func;
                int k = 5;
                while (k <= 10)
                {
                    double x = -10;
                    while (x <= 10)
                    {
                        func = (x * k + 1) / Math.Pow(k, 3);
                        Console.Write("{0,12}", Math.Round(func, 4));
                        x++;
                    }
                    k++;
                    Console.WriteLine();
                }
            }

            static void Func3()
            {
                double func;
                double x = -10;
                do
                {
                    int k = 5;
                    do
                    {
                        func = (x * k + 1) / Math.Pow(k, 3);
                        Console.Write("{0,12}", Math.Round(func, 4));
                        k++;
                    }
                    while (k <= 10);
                    x++;
                    Console.WriteLine();
                }
                while (x <= 10);
            }
        }

    }
}
