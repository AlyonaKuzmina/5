﻿using System;

namespace Кузьмина_Алена_лаб_5___2
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 0;
            double comp = 1;
            Console.WriteLine("Введите х=");
            double x = Convert.ToDouble(Console.ReadLine());
            int n = 5;


            for (int k = 1; k <= n; k++)
            {
                sum += (x * k + 1) / Math.Pow(-k, (3));
                comp *= (x * k + 1) / Math.Pow(-k, (3));
                Console.WriteLine($"sum({k})= {sum}");
                Console.WriteLine($"comp({k})= {comp}");
            }

            Console.WriteLine($"Сумма ряда равна {sum}");
            Console.WriteLine($"Произведение ряда равно {comp}");
        }
    }
}
